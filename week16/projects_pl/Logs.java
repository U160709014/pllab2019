import javax.print.DocFlavor.STRING;

public class Logs {

    //Properties of Logs
    String ip;
    String date;
    Integer loginTime;

    //Construction
    public Logs(String x,String y, Integer z){
        this.ip=x;
        this.date=y;
        this.loginTime=z;
    }

    //Getters
    public String getterDate() {
        return date;
    }
    public Integer getterLogin() {
        return loginTime;
    }
    public String getterIP() {
        return ip;
    }
    
    @Override
    public String toString() {
        return "LOG ["
                + "ip=" + ip 
                + ", date=" + date
                + ", loginTime=" + loginTime
                + "]";
    }
}