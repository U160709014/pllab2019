
import java.util.Comparator;
public class logsComporator implements Comparator<Logs> {

    @Override
    public int compare(Logs log1, Logs log2) {
 
        // all comparison
        int compareDate = log1.getterDate()
                .compareTo(log2.getterDate());
        int compareLogin = log1.getterLogin()
                .compareTo(log2.getterLogin());
 
        return ((compareDate == 0) ? compareLogin : compareDate);
    }
}