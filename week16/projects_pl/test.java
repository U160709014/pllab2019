import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.io.*;
import java.util.Scanner;

public class test {

    public static void main(String[] args) {
        List<Logs> listOfLogs = new ArrayList<Logs>();
        try {
            String fileName = "./pl.txt";
            File file = new File(fileName);
            FileInputStream fis = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            String line;
            String[] aLog=new String[3];
            while((line = br.readLine()) != null){
                aLog=line.split(" ");
                listOfLogs.add(new Logs(aLog[0],aLog[1],Integer.parseInt(aLog[2])));
            }
            br.close();


            //Sorting 
            Collections.sort(listOfLogs, 
                new logsComporator());
            //Reversing
            Collections.reverse(listOfLogs);


            Scanner input = new Scanner(System.in);  // Create a Scanner object
            System.out.println("Enter a date:");
            String newDate=input.next();
            Iterator<Logs> custIterator = 
                listOfLogs.iterator();

            //Objeleri tek tek alıp log değişkenine atıyor
            while (custIterator.hasNext()) {
                Logs log = custIterator.next();

                // İstenilen tarih olup olmadığını karşılaştırıyor
                if (log.getterDate().equals(newDate)) {
                    System.out.println(log.toString());
                    System.out.println(log.getterIP());
                    break;
                }
            }

        } catch (Exception e) {
            System.out.println("Something is wrong");
        }
        
    }
    
}

